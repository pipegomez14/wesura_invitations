#language: es
#author: fgomez@qvision.com.co

Característica: Invitaciones
  Yo como admin de Wesura
  quiero enviar invitaciones a mis clientes
  para poder lograr que compren mas el seguro de wesura


  @InvitacionWhatsapp
  Esquema del escenario: Abrir whatsapp en en la web y enviar invitaciones a los usuarios
    Dado que pipe abre whatsapp con los links dados
      | link   |
      | <link> |
    Cuando ingresa a invitar usuarios <posicion>
    Entonces verificamos que la invitación se haya realizado con éxito

    Ejemplos:
      | link                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | posicion |
      | https://api.whatsapp.com/send?phone=573104290109&text=*Hola Angela Rubio*%0D%0A%0D%0A😎Queremos%20que%20sigas%20protegido%20con%20Wesura%20😎%0D%0A%0D%0APor%20eso%20te%20damos%20un%20súper%20descuento%20para%20que%20lo%20uses%20en%20la%20renovación%20en%20el%20paso%20de%20pago.%20Solo%20debes%20responder%20esta%20corta%20encuesta:%20%20http://renueva.wesura.com.%0D%0A%0D%0A*Puedes%20renovar%20ahora%20mismo%20haciendo%20clic%20en*%20http%3A%2F%2Fwww.wesura.com%2Frenovacion/?utm_source%3DWhatsAppChat%26utm_medium%3DWhatsapp%26utm_campaign%3Drenovacion_whatsapp%26utm_content%3Drenovacion/%0D%0A%0D%0ASi%20*NO*%20deseas%20recibir%20m%C3%A1s%20informaci%C3%B3n%20sobre%20esta%20p%C3%B3liza%20haz%20clic%20en%20http://teesperamos.wesura.com/%0D%0A%0D%0ASi%20deseas%20cancelar%20estas%20notificaciones,%20puedes%20escribir%20*STOP*%20en%20cualquier%20momento.%0D%0A%0D%0AEquipo%20Wesura.%20🎈 | 1        |
      | https://api.whatsapp.com/send?phone=573045978514&text=*Hola Freddy Lopez*%0D%0A%0D%0A😎Queremos%20que%20sigas%20protegido%20con%20Wesura%20😎%0D%0A%0D%0APor%20eso%20te%20damos%20un%20súper%20descuento%20para%20que%20lo%20uses%20en%20la%20renovación%20en%20el%20paso%20de%20pago.%20Solo%20debes%20responder%20esta%20corta%20encuesta:%20%20http://renueva.wesura.com.%0D%0A%0D%0A*Puedes%20renovar%20ahora%20mismo%20haciendo%20clic%20en*%20http%3A%2F%2Fwww.wesura.com%2Frenovacion/?utm_source%3DWhatsAppChat%26utm_medium%3DWhatsapp%26utm_campaign%3Drenovacion_whatsapp%26utm_content%3Drenovacion/%0D%0A%0D%0ASi%20*NO*%20deseas%20recibir%20m%C3%A1s%20informaci%C3%B3n%20sobre%20esta%20p%C3%B3liza%20haz%20clic%20en%20http://teesperamos.wesura.com/%0D%0A%0D%0ASi%20deseas%20cancelar%20estas%20notificaciones,%20puedes%20escribir%20*STOP*%20en%20cualquier%20momento.%0D%0A%0D%0AEquipo%20Wesura.%20🎈 | 0        |
      | https://api.whatsapp.com/send?phone=573136631056&text=*Hola Juancho*%0D%0A%0D%0A😎Queremos%20que%20sigas%20protegido%20con%20Wesura%20😎%0D%0A%0D%0APor%20eso%20te%20damos%20un%20súper%20descuento%20para%20que%20lo%20uses%20en%20la%20renovación%20en%20el%20paso%20de%20pago.%20Solo%20debes%20responder%20esta%20corta%20encuesta:%20%20http://renueva.wesura.com.%0D%0A%0D%0A*Puedes%20renovar%20ahora%20mismo%20haciendo%20clic%20en*%20http%3A%2F%2Fwww.wesura.com%2Frenovacion/?utm_source%3DWhatsAppChat%26utm_medium%3DWhatsapp%26utm_campaign%3Drenovacion_whatsapp%26utm_content%3Drenovacion/%0D%0A%0D%0ASi%20*NO*%20deseas%20recibir%20m%C3%A1s%20informaci%C3%B3n%20sobre%20esta%20p%C3%B3liza%20haz%20clic%20en%20http://teesperamos.wesura.com/%0D%0A%0D%0ASi%20deseas%20cancelar%20estas%20notificaciones,%20puedes%20escribir%20*STOP*%20en%20cualquier%20momento.%0D%0A%0D%0AEquipo%20Wesura.%20🎈      | 0        |