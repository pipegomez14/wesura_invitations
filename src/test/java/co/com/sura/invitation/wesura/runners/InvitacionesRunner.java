package co.com.sura.invitation.wesura.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        snippets = SnippetType.CAMELCASE,
        features = "src/test/resources/features/invitaciones.feature",
        glue = {"co.com.sura.invitation.wesura.stepdefinitions"}
)
public class InvitacionesRunner {

}
