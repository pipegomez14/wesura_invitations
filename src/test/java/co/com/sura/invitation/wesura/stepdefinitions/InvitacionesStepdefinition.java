package co.com.sura.invitation.wesura.stepdefinitions;

import co.com.sura.invitation.wesura.models.WhatsappModel;
import co.com.sura.invitation.wesura.tasks.AbrirElNavegador;
import co.com.sura.invitation.wesura.tasks.AbrirLinksWhatsapp;
import co.com.sura.invitation.wesura.tasks.EnviarInvitacionsPorWhatsapp;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.pages.Pages;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class InvitacionesStepdefinition {

    @ManagedPages
    public Pages pages;

    private static String WELSON = "Welson";

    @Before
    public void inicializacion() {
        OnStage.setTheStage(new OnlineCast());

    }

    @Dado("^que pipe abre whatsapp con los links dados$")
    public void quePipeAbreWhatsappConLosLinksDados(List<WhatsappModel> whatsappModel) {

        pages.getConfiguration().getEnvironmentVariables().setProperty(ThucydidesSystemProperty.WEBDRIVER_BASE_URL.getPropertyName(), "https://api.whatsapp.com");
        theActorCalled(WELSON).wasAbleTo(AbrirElNavegador.enLaAplicacionDeWhatsapp());
        theActorInTheSpotlight().attemptsTo(AbrirLinksWhatsapp.paraInvitar(whatsappModel));

    }


    @Cuando("^ingresa a invitar usuarios (.*)$")
    public void ingresaAInvitarUsuarios(String posicion) {
        theActorInTheSpotlight().attemptsTo(EnviarInvitacionsPorWhatsapp.ahora(Integer.parseInt(posicion)));
    }

    @Entonces("^verificamos que la invitación se haya realizado con éxito$")
    public void verificamosQueLaInvitacionSeHayaRealizadoConExito() {

    }
}
