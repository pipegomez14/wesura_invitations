package co.com.sura.invitation.wesura.tasks;

import co.com.sura.invitation.wesura.models.WhatsappModel;
import co.com.sura.invitation.wesura.utils.Esperar;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class AbrirLinksWhatsapp implements Task {

    private static WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();

    private List<WhatsappModel> whatsappModel;

    public AbrirLinksWhatsapp(List<WhatsappModel> whatsappModel) {
        this.whatsappModel = whatsappModel;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Esperar.unMomento(2));
        WhatsappModel links = whatsappModel.get(0);
        driver.get(links.getLink());

    }

    public static AbrirLinksWhatsapp paraInvitar(List<WhatsappModel> whatsappModel) {
        return Tasks.instrumented(AbrirLinksWhatsapp.class, whatsappModel);
    }
}
