package co.com.sura.invitation.wesura.tasks;

import co.com.sura.invitation.wesura.userinterfaces.HomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirElNavegador implements Task {


    private HomePage homePage;


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(homePage));
    }

    public static AbrirElNavegador enLaAplicacionDeWhatsapp() {

        return Tasks.instrumented(AbrirElNavegador.class);
    }
}
