package co.com.sura.invitation.wesura.utils;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import org.awaitility.Awaitility;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static co.com.sura.invitation.wesura.utils.UtilidadTiempo.condicionExitosa;

public class Esperar implements Interaction {
    public static final Logger LOGGER = Logger.getLogger("Excepción");
    private int tiempo;

    public Esperar(int tiempo) {
        this.tiempo = tiempo;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            Awaitility.await().forever().pollInterval(tiempo, TimeUnit.SECONDS).until(condicionExitosa());
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Error " + e);
        }

    }

    public static Esperar unMomento(int tiempo) {

        return Tasks.instrumented(Esperar.class, tiempo);
    }

}