package co.com.sura.invitation.wesura.userinterfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class HomePage extends PageObject {

    public static final Target SEND = Target.the("boton para enviar invitaciones por whatsapp").located(By.id("action-button"));
    public static final Target ENVIAR_AHORA = Target.the("boton para enviar invitaciones dentro whatsapp").locatedBy("(//*[@class='weEq5'])[2]/button");


}
