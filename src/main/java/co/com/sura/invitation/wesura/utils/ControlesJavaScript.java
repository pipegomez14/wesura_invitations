package co.com.sura.invitation.wesura.utils;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.JavascriptExecutor;

public class ControlesJavaScript {

    public static void hacerClickEnElElemento(Actor actor, Target elemento) {
        JavascriptExecutor executor = (JavascriptExecutor) Serenity.getWebdriverManager().getCurrentDriver();
        executor.executeScript("arguments[0].click()", elemento.resolveFor(actor));
    }

    private ControlesJavaScript() {
    }
}
