package co.com.sura.invitation.wesura.tasks;

import co.com.sura.invitation.wesura.utils.ControlesJavaScript;
import co.com.sura.invitation.wesura.utils.Esperar;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.sura.invitation.wesura.userinterfaces.HomePage.ENVIAR_AHORA;
import static co.com.sura.invitation.wesura.userinterfaces.HomePage.SEND;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;


public class EnviarInvitacionsPorWhatsapp implements Task {

    private int posicion;

    public EnviarInvitacionsPorWhatsapp(int posicion) {
        this.posicion = posicion;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(WaitUntil.the(SEND, isVisible()));
        ControlesJavaScript.hacerClickEnElElemento(actor, SEND);

        if (posicion == 1) {
            actor.attemptsTo(Esperar.unMomento(15));
        } else {
            actor.attemptsTo(Esperar.unMomento(10));
        }

        actor.attemptsTo(WaitUntil.the(ENVIAR_AHORA, isVisible()));
        ControlesJavaScript.hacerClickEnElElemento(actor, ENVIAR_AHORA);
    }

    public static EnviarInvitacionsPorWhatsapp ahora(int posicion) {
        return Tasks.instrumented(EnviarInvitacionsPorWhatsapp.class, posicion);
    }


}
