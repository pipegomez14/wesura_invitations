<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" class="js serviceworker adownload cssanimations csstransitions webp webp-alpha webp-animation webp-lossless" dir="LTR" loc="en" lang="es"><head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>WhatsApp</title>
<meta name="viewport" content="width=device-width" />
<meta name="google" content="notranslate" />
<meta name="format-detection" content="telephone=no" />

<meta name="description" content="Quickly send and receive WhatsApp messages right from your computer." />
<meta name="og:description" content="Quickly send and receive WhatsApp messages right from your computer." />
<meta name="og:url" content="https://web.whatsapp.com/" />
<meta name="og:title" content="WhatsApp Web" />
<meta name="og:image" content="https://www.whatsapp.com/img/whatsapp-promo.png" />

<link id="favicon" rel="shortcut icon" href="/img/c5088e888c97ad440a61d247596f88e5.png" type="image/png" />
<link rel="apple-touch-icon" sizes="194x194" href="/apple-touch-icon.png" type="image/png" />

<link rel="stylesheet" href="/cssm_28c87aeefcae397ecb45d661e07c1d79.css" />

<style>
html, body, #app {
height: 100%;
width: 100%;
overflow: hidden;
padding: 0;
margin: 0;
}

#app {
position: absolute;
top: 0;
left: 0;
}

#startup, #initial_startup {
width: 100%;
height: 100%;
position: fixed;
background-color: #f2f2f2;

-moz-user-select: none;
-webkit-user-select: none;

display: flex;
align-items: center;
justify-content: center;
display: -webkit-box;
display: -webkit-flex;
-webkit-align-items: center;
-webkit-justify-content: center;
flex-direction: column;
-webkit-flex-direction: column;
}

.spinner-container {
-webkit-animation: rotate 2s linear infinite;
animation: rotate 2s linear infinite;
z-index: 2;
}

.spinner-path {
stroke-dasharray: 1,150; 
stroke-dashoffset: 0;
stroke: #acb9bf;
stroke-linecap: round;
-webkit-animation: dash 1.5s ease-in-out infinite;
animation: dash 1.5s ease-in-out infinite;
}

@keyframes rotate {
100% { transform: rotate(360deg); }
}
@-webkit-keyframes rotate{
100% { -webkit-transform: rotate(360deg); }
}

@keyframes dash {
0% {
stroke-dasharray: 1,150;  
stroke-dashoffset: 0;
}
50% {
stroke-dasharray: 90,150; 
stroke-dashoffset: -35;   
}
100% {
stroke-dasharray: 90,150; 
stroke-dashoffset: -124;  
}
}
@-webkit-keyframes dash {
0% {
stroke-dasharray: 1,150;  
stroke-dashoffset: 0;
}
50% {
stroke-dasharray: 90,150; 
stroke-dashoffset: -35;   
}
100% {
stroke-dasharray: 90,150; 
stroke-dashoffset: -124;  
}
}

.progress-container {
width: 360px;
position: fixed;
padding-top: 120px;
top: 50%;
left: 50%;
margin-left: -180px;
}

progress {
-webkit-appearance: none;
appearance: none;
width: 100%;
height: 3px;
border: none;
margin: 0;
color: #02d1a4;
background-color: #e0e4e5;
}

progress[value]::-webkit-progress-bar {
background-color: #e0e4e5;
}

progress[value]::-webkit-progress-value {
background-color: #02d1a4;
}

progress[value]::-moz-progress-bar {
background-color: #02d1a4;
}
</style>
<script id="progress_script_/vendor1.750a4a61979ae2f58517.js" type="text/javascript" charset="utf-8" async="" src="/vendor1.750a4a61979ae2f58517.js"></script><script id="progress_script_/vendor2.120848e076bd9bf63061.js" type="text/javascript" charset="utf-8" async="" src="/vendor2.120848e076bd9bf63061.js"></script><script id="progress_script_/app.4a9a6069eb198d7740bf.js" type="text/javascript" charset="utf-8" async="" src="/app.4a9a6069eb198d7740bf.js"></script><script id="progress_script_/app2.465f0a6071edd3ca3113.js" type="text/javascript" charset="utf-8" async="" src="/app2.465f0a6071edd3ca3113.js"></script><style id="asset-style" type="text/css"></style><script type="text/javascript" charset="utf-8" async="" src="/svg.21a0cfb383dc0bcaa38b.js"></script><script type="text/javascript" charset="utf-8" async="" src="/app2.465f0a6071edd3ca3113.js"></script><script type="text/javascript" charset="utf-8" async="" src="/locales/es.c9bf4624edad947da059.js"></script><script type="text/javascript" charset="utf-8" async="" src="/locales/en.fa66d856bbdb4dcec395.js"></script></head>
<body class="web">
<div id="app"><div class="_1FKgS app-wrapper-web"><div id="startup"><svg xmlns="http://www.w3.org/2000/svg" class="_1UDDE" width="50" height="50" viewBox="0 0 44 44"><circle class="_3GbTq _3AnXT" cx="22" cy="22" r="20" fill="none" stroke-width="4"/></svg><div class="Pg7Si"><progress value="80" max="100" dir="ltr"></progress></div></div></div></div>



<script src="/progress.85368d5f9104b9aeeadfbc0ecb65d3ac.js"></script>

<div id="hard_expire_time" data-time="1560300630.109"></div>



</body></html>